FROM python:3.11
WORKDIR /todo-list
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
COPY ./requirements.txt /todo-list/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /todo-list/requirements.txt
COPY ./app /todo-list/app
COPY ./db /todo-list/app/db
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]