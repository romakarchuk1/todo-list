from app.schemas import STATUS
import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from http import HTTPStatus
from app.db import Base
from app.main import app, get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///tests/test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


@pytest.fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.fixture()
def add_item():
    client.post("/api/tasks", json={"title": "test", "description": "test"})


@pytest.fixture()
def delete_item():
    client.delete("/api/tasks/1")


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_add_task(test_db):
    response = client.post("/api/tasks", json={"title": "test", "description": "test"})
    assert response.status_code == HTTPStatus.CREATED
    assert response.json()["id"] == 1


def test_add_task_validation_error(test_db):
    response = client.post("/api/tasks", json={"description": "test"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_get_all_task(test_db, add_item):
    response = client.get("/api/tasks")
    assert response.status_code == HTTPStatus.OK


def test_get_all_task_if_empty(test_db):
    response = client.get("/api/tasks")
    assert response.status_code == HTTPStatus.OK


def test_get_task(test_db, add_item):
    response = client.get("/api/tasks/1")
    assert response.status_code == HTTPStatus.OK


def test_get_task_not_fount(test_db, add_item):
    response = client.get("/api/tasks/2")
    assert response.status_code == HTTPStatus.NOT_FOUND


def test_update_task(test_db, add_item):
    response = client.patch("/api/tasks/1")
    assert response.status_code == HTTPStatus.OK
    response = client.get("/api/tasks/1")
    assert response.json()["status"] == STATUS.IN_PROGRESS.value
    response = client.patch("/api/tasks/1")
    assert response.status_code == HTTPStatus.OK
    response = client.get("/api/tasks/1")
    assert response.json()["status"] == STATUS.DONE.value


def test_delete_task(test_db, add_item):
    response = client.delete("/api/tasks/1")
    assert response.status_code == HTTPStatus.NO_CONTENT


def test_delete_task_not_found(test_db, add_item):
    response = client.delete("/api/tasks/2")
    assert response.status_code == HTTPStatus.NOT_FOUND
