
# ToDo list application

A CRUD service in Python for to-do list that will support reading list, adding new items, change items statuses, delete items


## Stack
- FastApi
- uvicorn
- SQLite
- SQLAlchemy
## Installation

Build an docker image

```bash
  docker build -t myimage .
```
Run docker contaner. 
After application startup is complete - server will be running on http://127.0.0.1.


```bash
  docker run -d --name mycontainer -p 80:80 myimage
```

To stop an docker image

```bash
  docker stop mycontainer
```
## API Reference
API consits of 5 endpoints.

You can check endpoints description and how to use them at http://127.0.0.1/docs

- Add task. Method: POST, Endpoint: /tasks. You can add task with name and description. If you don't add description, it will be empty. If you don't add name, you will get an error.
- Read all tasks. Method: GET, Endpoint: /tasks
- Read single task. Method: GET, Endpoint: /tasks/{task_id}
- Change status of single task. Method: PATCH, Endpoint: /tasks/{task_id}
  
  Task has of these statuses:
   - "Pending"
   - "In Progress"
   - "Done"
    
    Default status of created task is "Pending". To change status - use PATCH operation which will change status to "In Progress". If task is done - use PATCH operation one more time, this will change status to "Done".
- Delete single task. Method: DELETE, Endpoint: /tasks/{task_id}





## Tests
Tests located at .test directory

You can run it by:
```bash
pytest tests.py
```