from pydantic import BaseModel
from datetime import datetime
from enum import Enum
from fastapi import Form
from typing import Union


class STATUS(str, Enum):
    PENDING = "Pending"
    IN_PROGRESS = "In Progress"
    DONE = "Done"


class BaseTaskRequestModel(BaseModel):
    id: int = Form(...)

    class Config:
        orm_mode = True


class TaskPostRequestModel(BaseModel):
    title: str
    description: Union[str, None] = None


class BaseTaskResponseModel(BaseModel):
    id: int

    class Config:
        orm_mode = True


class TaskGetResponseModel(BaseTaskResponseModel):
    status: STATUS
    title: str
    description: Union[str, None]
    created_at: datetime

