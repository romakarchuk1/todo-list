from app import models
from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
from datetime import datetime
from app.schemas import BaseTaskResponseModel, TaskPostRequestModel, TaskGetResponseModel, STATUS
from typing import List
from app.db import get_db
from fastapi.responses import RedirectResponse


tags_metadata = [
    {
        "name": "Tasks",
        "description": "CRUD Operations with tasks.",
    },
]

app = FastAPI(
    title="ToDo List",
    description="A CRUD service in Python for to-do list that will support reading list, "
                "adding new items, change items statuses, delete items",
    version="0.0.1",
)


@app.get("/", response_class=RedirectResponse, include_in_schema=False)
def redirect_to_docs():
    return "/docs"


@app.post("/api/tasks", response_model=BaseTaskResponseModel, status_code=201, tags=["Tasks"])
def add_task(req_data: TaskPostRequestModel, db: Session = Depends(get_db)):
    """ Add new task """
    task = models.Task(title=req_data.title, description=req_data.description,
                       status=STATUS.PENDING.value, created_at=datetime.now())
    db.add(task)
    db.commit()
    return task


@app.get("/api/tasks/{task_id}", response_model=TaskGetResponseModel, status_code=200, tags=["Tasks"])
def get_task(task_id: int, db: Session = Depends(get_db)):
    """ Get task by id """
    task = db.query(models.Task).filter(models.Task.id == task_id).first()
    if not task:
        raise HTTPException(status_code=404, detail="Task not found")
    return task


@app.patch("/api/tasks/{task_id}", response_model=BaseTaskResponseModel, status_code=200, tags=["Tasks"])
def update_task(task_id: int, db: Session = Depends(get_db)):
    """ Update task status """
    task = db.query(models.Task).filter(models.Task.id == task_id).first()
    if not task:
        raise HTTPException(status_code=404, detail="Task not found")
    if task.status == STATUS.PENDING.value:
        task.status = STATUS.IN_PROGRESS.value
    elif task.status == STATUS.IN_PROGRESS.value:
        task.status = STATUS.DONE.value
    db.commit()
    return task


@app.delete("/api/tasks/{task_id}", status_code=204, tags=["Tasks"])
def delete_task(task_id, db: Session = Depends(get_db)):
    """ Delete task by id """
    task = db.query(models.Task).filter(models.Task.id == task_id).first()
    if not task:
        raise HTTPException(status_code=404, detail="Task not found")
    db.delete(task)
    db.commit()
    return {}


@app.get("/api/tasks", response_model=List[TaskGetResponseModel], status_code=200, tags=["Tasks"])
def get_all_tasks(db: Session = Depends(get_db)):
    """ Get all tasks """
    tasks = db.query(models.Task).all()
    return tasks
