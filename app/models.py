from app.db import Base
from datetime import datetime
from sqlalchemy import Integer, Enum
from app.schemas import STATUS
from sqlalchemy import Column, String, DateTime


class Task(Base):
    __tablename__ = "tasks"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    description = Column(String)
    status = Column(Enum(STATUS))
    created_at = Column(DateTime, default=datetime.now())
